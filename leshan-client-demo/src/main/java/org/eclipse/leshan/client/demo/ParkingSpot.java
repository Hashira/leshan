package org.eclipse.leshan.client.demo;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;

import org.eclipse.leshan.client.request.ServerIdentity;
import org.eclipse.leshan.client.resource.BaseInstanceEnabler;
import org.eclipse.leshan.core.model.ObjectModel;
import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.response.ExecuteResponse;
import org.eclipse.leshan.core.response.ReadResponse;
import org.eclipse.leshan.core.response.WriteResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rpi.sensehat.api.SenseHat;
import rpi.sensehat.api.dto.Color;
import rpi.sensehat.api.dto.JoystickEvent;

public class ParkingSpot extends BaseInstanceEnabler {
  // Static value for object ID
  public static final int OBJECT_ID = 32700;
  // Static values for resource items
  private static final int RES_PARKING_SPOT_ID = 32800;
  private static final int RES_PARKING_SPOT_STATE = 32801;
  private static final int RES_LOT_NAME = 32802;
  private static final List<Integer> supportedResources = Arrays.asList(RES_PARKING_SPOT_ID, RES_PARKING_SPOT_STATE,
      RES_LOT_NAME);
  private static final String RES_STATE_FREE = "Free";
  private static final String RES_STATE_RESERVED = "Reserved";
  private static final String RES_STATE_OCCUPIED = "Occupied";
  private static final Collection<String> RES_VALID_STATES = new HashSet<>(
      Arrays.asList(RES_STATE_FREE, RES_STATE_OCCUPIED, RES_STATE_RESERVED));
  private static final Map<String, Color[]> RES_STATE_COLOR = new HashMap<String, Color[]>() {
    private static final long serialVersionUID = 3844462182731417274L;

    {
      put(RES_STATE_FREE, fillMatrix(Color.GREEN));
      put(RES_STATE_RESERVED, fillMatrix(Color.of(255, 165, 0)));
      put(RES_STATE_OCCUPIED, fillMatrix(Color.RED));
    }
  };
  // Variables storing current values.
  private String vParkingSpotId = "";
  // Free,Reserved,Occupied
  private String vParkingSpotState = "";
  private String vLotName = "";

  private static final Logger LOG = LoggerFactory.getLogger(ParkingSpot.class);
  private static final ExecutorService EXECUTOR = new ForkJoinPool();

  private final SenseHat senseHat;

  public ParkingSpot() {
    this.senseHat = null;
  }

  public ParkingSpot(final String id) {
    this.senseHat = new SenseHat();

    setParkingSpotId(id);
    setParkingSpotState(RES_STATE_FREE);

    EXECUTOR.submit(() -> listenForEvents());
  }

  @Override
  public synchronized ReadResponse read(final ServerIdentity identity, final int resourceId) {
    switch (resourceId) {
    case RES_PARKING_SPOT_ID:
      return ReadResponse.success(resourceId, vParkingSpotId);
    case RES_PARKING_SPOT_STATE:
      return ReadResponse.success(resourceId, vParkingSpotState);
    case RES_LOT_NAME:
      return ReadResponse.success(resourceId, vLotName);
    default:
      return super.read(identity, resourceId);
    }
  }

  @Override
  public WriteResponse write(final ServerIdentity identity, final int resourceId, final LwM2mResource value) {
    switch (resourceId) {
    case RES_PARKING_SPOT_STATE:
      return setParkingSpotState((String) value.getValue());
    case RES_LOT_NAME:
      return setLotName((String) value.getValue());
    default:
      return super.write(identity, resourceId, value);
    }
  }

  @Override
  public synchronized ExecuteResponse execute(final ServerIdentity identity, final int resourceId,
      final String params) {
    switch (resourceId) {
    default:
      return super.execute(identity, resourceId, params);
    }
  }

  @Override
  public List<Integer> getAvailableResourceIds(final ObjectModel model) {
    return supportedResources;
  }

  private synchronized WriteResponse setParkingSpotId(final String value) {
    if (vParkingSpotId != value) {
      vParkingSpotId = value;
      fireResourcesChange(RES_PARKING_SPOT_ID);
    }
    return WriteResponse.success();
  }

  private synchronized WriteResponse setParkingSpotState(final String value) {
    if (!RES_VALID_STATES.contains(value)) {
      return WriteResponse.badRequest("invalid state");
    }
    if (vParkingSpotState != value) {
      vParkingSpotState = value;
      fireResourcesChange(RES_PARKING_SPOT_STATE);

      Color[] newColor = RES_STATE_COLOR.get(value);
      senseHat.ledMatrix.setPixels(newColor);
    }
    return WriteResponse.success();
  }

  private synchronized WriteResponse setLotName(final String value) {
    if (vLotName != value) {
      vLotName = value;
      fireResourcesChange(RES_LOT_NAME);
    }
    return WriteResponse.success();
  }

  private void listenForEvents() {
    final JoystickEvent event = senseHat.joystick.waitForEvent();

    switch (event.getDirection()) {
    case DOWN:
    case LEFT:
      setParkingSpotState(RES_STATE_FREE);
      break;
    case MIDDLE:
      setParkingSpotState(RES_STATE_RESERVED);
      break;
    case UP:
    case RIGHT:
      setParkingSpotState(RES_STATE_OCCUPIED);
      break;
    }

    EXECUTOR.submit(() -> listenForEvents());
  }

  private static Color[] fillMatrix(final Color color) {
    Color[] matrix = new Color[64];
    for (int i = 0; i < 64; ++i) {
      matrix[i] = color;
    }
    return matrix;
  }
}
