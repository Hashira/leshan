package org.eclipse.leshan.client.demo;

public class LocationBuilder {
    private Boolean enabled = false;
    private Double latitude = 0.0;
    private Double longitude = 0.0;

    public LocationBuilder() {
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Location build() {
        if (enabled) {
            return new Location(latitude, longitude);
        }
        return null;
    }

    public LocationBuilder setEnabled(final Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public LocationBuilder setLatitude(final Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public LocationBuilder setLongitude(final Double longitude) {
        this.longitude = longitude;
        return this;
    }
}