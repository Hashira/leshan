package org.eclipse.leshan.client.demo;

import com.hopding.jrpicam.exceptions.FailedToRunRaspistillException;

public class VehicleCounterBuilder {
    private Boolean enabled = false;
    private String id = "";

    public VehicleCounterBuilder() {
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public VehicleCounterBuilder setEnabled(final Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public String getId() {
        return id;
    }

    public VehicleCounterBuilder setId(final String id) {
        this.id = id;
        return this;
    }

    public VehicleCounter build() throws FailedToRunRaspistillException {
        if (enabled) {
            return new VehicleCounter(id);
        }
        return null;
    }
}