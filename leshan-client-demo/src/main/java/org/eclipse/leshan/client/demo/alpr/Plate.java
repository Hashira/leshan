package org.eclipse.leshan.client.demo.alpr;

public class Plate {
    private String plate;
    private Float confidence;
    private Integer matchesTemplate;

    private Plate() {
    }

    public String getPlate() {
        return plate;
    }

    public Float getConfidence() {
        return confidence;
    }

    public Integer getMatchesTemplate() {
        return matchesTemplate;
    }

    @Override
    public String toString() {
        return "Plate [plate=" + plate + ", matchesTemplate=" + matchesTemplate + ", confidence=" + confidence + "]";
    }

}