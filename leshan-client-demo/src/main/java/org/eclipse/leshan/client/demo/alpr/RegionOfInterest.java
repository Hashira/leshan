package org.eclipse.leshan.client.demo.alpr;

public class RegionOfInterest {
    private Integer x;
    private Integer y;
    private Integer width;
    private Integer height;

    private RegionOfInterest() {
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "RegionOfInterest [height=" + height + ", width=" + width + ", x=" + x + ", y=" + y + "]";
    }

}