package org.eclipse.leshan.client.demo.alpr;

import java.util.List;

public class Results {
    private Long epochTime;
    private Integer imgWidth;
    private Integer imgHeight;
    private Float processingTimeMs;

    private List<PlateResult> results;
    private List<RegionOfInterest> regionsOfInterest;

    private Results() {
    }

    public Long getEpochTime() {
        return epochTime;
    }

    public Integer getImgWidth() {
        return imgWidth;
    }

    public Integer getImgHeight() {
        return imgHeight;
    }

    public Float getProcessingTimeMs() {
        return processingTimeMs;
    }

    public List<PlateResult> getResults() {
        return results;
    }

    public List<RegionOfInterest> getRegionsOfInterest() {
        return regionsOfInterest;
    }

    @Override
    public String toString() {
        return "Results [epochTime=" + epochTime + ", imgHeight=" + imgHeight + ", imgWidth=" + imgWidth + ", results="
                + results + ", regionsOfInterest=" + regionsOfInterest + ", processingTimeMs=" + processingTimeMs + "]";
    }

}