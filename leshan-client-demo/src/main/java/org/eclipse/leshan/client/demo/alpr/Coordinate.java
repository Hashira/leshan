package org.eclipse.leshan.client.demo.alpr;

public class Coordinate {
    private Integer x;
    private Integer y;

    private Coordinate() {
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

    @Override
    public String toString() {
        return "Coordinate [x=" + x + ", y=" + y + "]";
    }

}