package org.eclipse.leshan.client.demo.alpr;

import java.util.List;

public class PlateResult {
    private Integer requestedTopn;
    private List<Plate> candidates;
    private Float processingTimeMs;
    private List<Coordinate> coordinates;
    private Integer plateIndex;
    private Integer regionConfidence;
    private String region;

    private PlateResult() {
    }

    public Integer getRequestedTopn() {
        return requestedTopn;
    }

    public List<Plate> getCandidates() {
        return candidates;
    }

    public Float getProcessingTimeMs() {
        return processingTimeMs;
    }

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

    public Integer getPlateIndex() {
        return plateIndex;
    }

    public Integer getRegionConfidence() {
        return regionConfidence;
    }

    public String getRegion() {
        return region;
    }

    @Override
    public String toString() {
        return "PlateResult [plateIndex=" + plateIndex + ", coordinates=" + coordinates + ", processingTimeMs="
                + processingTimeMs + ", region=" + region + ", regionConfidence=" + regionConfidence
                + ", requestedTopn=" + requestedTopn + ", candidates=" + candidates + "]";
    }

}