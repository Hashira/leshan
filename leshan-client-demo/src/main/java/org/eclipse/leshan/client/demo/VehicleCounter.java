package org.eclipse.leshan.client.demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hopding.jrpicam.RPiCamera;
import com.hopding.jrpicam.enums.Exposure;
import com.hopding.jrpicam.exceptions.FailedToRunRaspistillException;

import org.apache.commons.io.IOUtils;
import org.eclipse.leshan.client.demo.alpr.Plate;
import org.eclipse.leshan.client.demo.alpr.PlateResult;
import org.eclipse.leshan.client.demo.alpr.Results;
import org.eclipse.leshan.client.request.ServerIdentity;
import org.eclipse.leshan.client.resource.BaseInstanceEnabler;
import org.eclipse.leshan.core.model.ObjectModel;
import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.response.ExecuteResponse;
import org.eclipse.leshan.core.response.ReadResponse;
import org.eclipse.leshan.core.response.WriteResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VehicleCounter extends BaseInstanceEnabler {
  // Static value for object ID
  public static final int OBJECT_ID = 32701;
  // Static values for resource items
  private static final int RES_VEHICLE_COUNTER_ID = 32800;
  private static final int RES_LOT_NAME = 32802;
  private static final int RES_VEHICLE_COUNTER = 32803;
  private static final int RES_LAST_PLATE = 32804;
  private static final int RES_DIRECTION = 32805;
  private static final List<Integer> supportedResources = Arrays.asList(RES_VEHICLE_COUNTER_ID, RES_LOT_NAME,
      RES_VEHICLE_COUNTER, RES_LAST_PLATE, RES_DIRECTION);
  // Variables storing current values.
  private String vVehicleCounterId = "";
  private String vLotName = "";
  private int vVehicleCounter = 0;
  private String vLastPlate = "";
  // 1: Enter, 0: Exit
  private int vDirection = 0;

  private static final Logger LOG = LoggerFactory.getLogger(VehicleCounter.class);
  private static final ExecutorService EXECUTOR = new ForkJoinPool();

  private final RPiCamera piCamera;
  private final Gson gson;
  private final Set<String> inside;

  public VehicleCounter() {
    this.piCamera = null;
    this.gson = null;
    this.inside = null;
  }

  public VehicleCounter(final String id) throws FailedToRunRaspistillException {
    piCamera = new RPiCamera(System.getProperty("java.io.tmpdir")).setQuality(100).setExposure(Exposure.AUTO)
        .setTimeout(512).setWidth(3280).setHeight(2464);
    gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
    inside = new HashSet<>();

    setVehicleCounterId(id);

    EXECUTOR.submit(() -> retrieveImage());
  }

  @Override
  public synchronized ReadResponse read(final ServerIdentity identity, final int resourceId) {
    switch (resourceId) {
    case RES_VEHICLE_COUNTER_ID:
      return ReadResponse.success(resourceId, vVehicleCounterId);
    case RES_LOT_NAME:
      return ReadResponse.success(resourceId, vLotName);
    case RES_VEHICLE_COUNTER:
      return ReadResponse.success(resourceId, vVehicleCounter);
    case RES_LAST_PLATE:
      return ReadResponse.success(resourceId, vLastPlate);
    case RES_DIRECTION:
      return ReadResponse.success(resourceId, vDirection);
    default:
      return super.read(identity, resourceId);
    }
  }

  @Override
  public WriteResponse write(final ServerIdentity identity, final int resourceId, final LwM2mResource value) {
    switch (resourceId) {
    case RES_LOT_NAME:
      return setLotName((String) value.getValue());
    default:
      return super.write(identity, resourceId, value);
    }
  }

  @Override
  public synchronized ExecuteResponse execute(final ServerIdentity identity, final int resourceId,
      final String params) {
    switch (resourceId) {
    default:
      return super.execute(identity, resourceId, params);
    }
  }

  @Override
  public List<Integer> getAvailableResourceIds(final ObjectModel model) {
    return supportedResources;
  }

  private synchronized WriteResponse setVehicleCounterId(final String value) {
    if (vVehicleCounterId != value) {
      vVehicleCounterId = value;
      fireResourcesChange(RES_VEHICLE_COUNTER_ID);
    }
    return WriteResponse.success();
  }

  private synchronized WriteResponse setLotName(final String value) {
    if (vLotName != value) {
      vLotName = value;
      fireResourcesChange(RES_LOT_NAME);
    }
    return WriteResponse.success();
  }

  private synchronized WriteResponse setVehicleCounter(final int value) {
    if (vVehicleCounter != value) {
      vVehicleCounter = value;
      fireResourcesChange(RES_VEHICLE_COUNTER);
    }
    return WriteResponse.success();
  }

  private synchronized WriteResponse setLastPlate(final String value) {
    if (vLastPlate != value) {
      vLastPlate = value;
      fireResourcesChange(RES_LAST_PLATE);
    }
    return WriteResponse.success();
  }

  private synchronized WriteResponse setDirection(final int value) {
    if (vDirection != value) {
      vDirection = value;
      fireResourcesChange(RES_DIRECTION);
    }
    return WriteResponse.success();
  }

  private synchronized void updateStatus(final List<String> plates) {
    if (Collections.disjoint(plates, inside)) {
      inside.addAll(plates);
      setDirection(1);
    } else {
      inside.removeAll(plates);
      setDirection(0);
    }

    setLastPlate(plates.get(0));
    setVehicleCounter(vVehicleCounter + 1);

    LOG.info("Current state: {} ", this);
  }

  private void processImage(final File image) {
    try {
      final Process process = new ProcessBuilder(Arrays.asList("alpr", "-c", "eu", "-j", image.getAbsolutePath()))
          .start();
      process.waitFor();
      image.delete();

      try (final BufferedReader outputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
          final BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()))) {
        final Results results = gson.fromJson(outputReader, Results.class);
        final String error = IOUtils.toString(errorReader);

        LOG.info("ALPR results: {} ; error: {}", results, error);

        if (results != null) {
          results.getResults().stream().map(PlateResult::getCandidates).map(List::stream)
              .map((stream) -> stream.map(Plate::getPlate).collect(Collectors.toList())).forEach(this::updateStatus);
        }
      }
    } catch (final IOException e) {
      LOG.error("Caught {}", e);
      throw new RuntimeException(e);
    } catch (final InterruptedException e) {
      LOG.error("Caught {}", e);
      return;
    }
  }

  private void retrieveImage() {
    try {
      final File image = File.createTempFile("alpr", ".jpg");
      ImageIO.write(piCamera.takeBufferedStill(), "jpg", image);
      EXECUTOR.submit(() -> processImage(image));

      EXECUTOR.submit(() -> retrieveImage());
    } catch (final IOException e) {
      LOG.error("Caught {}", e);
      throw new RuntimeException(e);
    } catch (final InterruptedException e) {
      LOG.error("Caught {}", e);
      return;
    }
  }

  @Override
  public String toString() {
    return "VehicleCounter [vDirection=" + vDirection + ", vLastPlate=" + vLastPlate + ", vLotName=" + vLotName
        + ", vVehicleCounter=" + vVehicleCounter + ", vVehicleCounterId=" + vVehicleCounterId + "]";
  }

}
