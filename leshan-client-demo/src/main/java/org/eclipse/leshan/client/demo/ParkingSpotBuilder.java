package org.eclipse.leshan.client.demo;

public class ParkingSpotBuilder {
    private Boolean enabled = false;
    private String id = "";

    public ParkingSpotBuilder() {
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public ParkingSpotBuilder setEnabled(final Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public String getId() {
        return id;
    }

    public ParkingSpotBuilder setId(final String id) {
        this.id = id;
        return this;
    }

    public ParkingSpot build() {
        if (enabled) {
            return new ParkingSpot(id);
        }
        return null;
    }
}