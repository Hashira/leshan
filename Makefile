clean:
	mvn clean

build:
	mvn install -Dmaven.test.skip=true

clean.build:
	mvn clean install -Dmaven.test.skip=true

client.run:
	java -jar leshan-client-demo/target/leshan-client-demo-1.0.0-SNAPSHOT-jar-with-dependencies.jar -mdns $(ARGS)

server.run:
	java -jar leshan-server-demo/target/leshan-server-demo-1.0.0-SNAPSHOT-jar-with-dependencies.jar -mdns $(ARGS)
