package org.eclipse.leshan.server.demo.servlet;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.eclipse.leshan.Link;
import org.eclipse.leshan.core.node.LwM2mPath;
import org.eclipse.leshan.server.californium.LeshanServer;
import org.eclipse.leshan.server.demo.repository.ReservationRepository;
import org.eclipse.leshan.server.registration.Registration;
import org.glassfish.jersey.uri.UriTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service HTTP REST API calls.
 */
public class ParkingLotServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(ParkingLotServlet.class);

    private static final String NOT_FOUND = "{\"error\":\"not_found\"}";

    private static final int OBJ_ID_PARKING_SPOT = 32700;
    private static final int OBJ_ID_VEHICLE_COUNTER = 32701;

    private static final long serialVersionUID = 1L;
    private static final UriTemplate parkingSpotsTemplate = new UriTemplate("/parkingSpots");
    private static final UriTemplate parkingLotsTemplate = new UriTemplate("/parkingLots");
    private static final UriTemplate vehicleCountersTemplate = new UriTemplate("/vehicleCounters");

    private final LeshanServer server;
    private final String lotName;
    private final Gson gson;

    public ParkingLotServlet(final String lotName, final LeshanServer server) {
        this.server = server;
        this.lotName = lotName;

        GsonBuilder gsonBuilder = new GsonBuilder();
        this.gson = gsonBuilder.create();
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        final Map<String, String> parameters = new HashMap<>();
        final String prePath = req.getPathInfo();
        final String path = prePath == null ? "" : prePath.replaceAll("/$", "");

        if (path.isEmpty()) {
            notFound(resp);

            return;
        }

        final Iterator<Registration> registrations = server.getRegistrationService().getAllRegistrations();

        final Set<String> spots = new HashSet<>();
        final Set<String> counters = new HashSet<>();
        final Set<String> lots = new HashSet<>();
        lots.add(lotName);

        while (registrations.hasNext()) {
            final Registration registration = registrations.next();

            for (final Link link : registration.getObjectLinks()) {
                final Integer objectId = (new LwM2mPath(link.getUrl())).getObjectId();
                if (objectId == null) {
                    continue;
                }

                switch (objectId) {
                case OBJ_ID_PARKING_SPOT:
                    spots.add(registration.getEndpoint());
                    break;
                case OBJ_ID_VEHICLE_COUNTER:
                    counters.add(registration.getEndpoint());
                    break;
                }
            }
        }

        if (parkingLotsTemplate.match(path, parameters)) {
            found(resp, lots);

            return;
        }

        if (parkingSpotsTemplate.match(path, parameters)) {
            found(resp, spots);

            return;
        }

        if (vehicleCountersTemplate.match(path, parameters)) {
            found(resp, counters);

            return;
        }

        notFound(resp);
    }

    private void found(final HttpServletResponse resp, final Object result) throws IOException {
        final String json = gson.toJson(result);
        resp.setContentType("application/json");
        resp.getOutputStream().write(json.getBytes(StandardCharsets.UTF_8));
        resp.setStatus(HttpServletResponse.SC_OK);
    }

    private void notFound(final HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.getOutputStream().write(NOT_FOUND.getBytes(StandardCharsets.UTF_8));
        resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }
}