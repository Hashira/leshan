package org.eclipse.leshan.server.demo.repository;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.eclipse.leshan.server.demo.model.Reservation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class ReservationRepositoryImpl implements ReservationRepository {
    final private SessionFactory sessionFactory;

    public ReservationRepositoryImpl(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Reservation> get() {
        Transaction transaction = null;
        try (final Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            final CriteriaQuery<Reservation> criteria = session.getCriteriaBuilder().createQuery(Reservation.class);
            final CriteriaQuery<Reservation> query = criteria.select(criteria.from(Reservation.class));

            List<Reservation> result = session.createQuery(query).getResultList();
            transaction.commit();

            return result;
        } catch (final Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }

            throw e;
        }
    }

    private <E> E getByField(final String field, final Object value, Function<Query<Reservation>, E> f) {
        Transaction transaction = null;
        try (final Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            final CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            final CriteriaQuery<Reservation> criteria = criteriaBuilder.createQuery(Reservation.class);
            final Root<Reservation> root = criteria.from(Reservation.class);
            final CriteriaQuery<Reservation> query = criteria.select(root)
                    .where(criteriaBuilder.equal(root.get(field), value));

            final E result = f.apply(session.createQuery(query));
            transaction.commit();

            return result;
        } catch (final Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }

            throw e;
        }
    }

    public Optional<Reservation> getByID(final Long id) {
        return getByField("id", id, (query) -> query.getResultStream().findFirst());
    }

    public List<Reservation> getByPlate(final String name) {
        return getByField("plateNumber", name, (query) -> query.getResultList());
    }

    public List<Reservation> getBySpot(final String name) {
        return getByField("spotName", name, (query) -> query.getResultList());
    }

    public List<Reservation> getByLot(final String name) {
        return getByField("lotName", name, (query) -> query.getResultList());
    }

    public void save(final Reservation reservation) {
        Transaction transaction = null;
        try (final Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            session.save(reservation);

            transaction.commit();
        } catch (final Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }

            throw e;
        }
    }

    public void update(final Reservation reservation) {
        Transaction transaction = null;
        try (final Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            session.update(reservation);

            transaction.commit();
        } catch (final Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }

            throw e;
        }
    }

    public void delete(final Reservation reservation) {
        Transaction transaction = null;
        try (final Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            session.delete(reservation);

            transaction.commit();
        } catch (final Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }

            throw e;
        }
    }
}