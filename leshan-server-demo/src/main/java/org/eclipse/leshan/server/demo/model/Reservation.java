package org.eclipse.leshan.server.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "plateNumber", "spotName", "lotName" }) })
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String plateNumber;
    private String spotName;
    private String lotName;

    public Reservation() {

    }

    public Reservation withID(final long id) {
        return new Reservation(id, this.plateNumber, this.spotName, this.lotName);
    }

    public Reservation(final Long id, final String plateNumber, final String spotName, final String lotName) {
        this.id = id;
        this.plateNumber = plateNumber;
        this.spotName = spotName;
        this.lotName = lotName;
    }

    public Reservation(final String plateNumber, final String spotName, final String lotName) {
        this.plateNumber = plateNumber;
        this.spotName = spotName;
        this.lotName = lotName;
    }

    public Long getId() {
        return id;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public String getSpotName() {
        return spotName;
    }

    public String getLotName() {
        return lotName;
    }

    @Override
    public String toString() {
        return "Reservation [id=" + id + ", lotName=" + lotName + ", plateNumber=" + plateNumber + ", spotName="
                + spotName + "]";
    }

}