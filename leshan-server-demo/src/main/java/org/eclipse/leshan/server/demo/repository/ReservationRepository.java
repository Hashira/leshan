package org.eclipse.leshan.server.demo.repository;

import java.util.List;
import java.util.Optional;

import org.eclipse.leshan.server.demo.model.Reservation;

public interface ReservationRepository {
    List<Reservation> get();

    Optional<Reservation> getByID(final Long id);

    List<Reservation> getByPlate(final String name);

    List<Reservation> getBySpot(final String name);

    List<Reservation> getByLot(final String name);

    void save(final Reservation reservation);

    void update(final Reservation reservation);

    void delete(final Reservation reservation);
}