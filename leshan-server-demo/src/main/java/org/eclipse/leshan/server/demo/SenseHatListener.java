package org.eclipse.leshan.server.demo;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.eclipse.leshan.core.node.LwM2mNode;
import org.eclipse.leshan.core.node.LwM2mNodeVisitor;
import org.eclipse.leshan.core.node.LwM2mObject;
import org.eclipse.leshan.core.node.LwM2mObjectInstance;
import org.eclipse.leshan.core.node.LwM2mPath;
import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.server.registration.Registration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rpi.sensehat.api.SenseHat;
import rpi.sensehat.api.dto.Color;

public class SenseHatListener implements ParkingLotListener {

    private static final Logger LOG = LoggerFactory.getLogger(SenseHatListener.class);
    private static final ExecutorService EXECUTOR = Executors.newSingleThreadExecutor();

    private static final int OBJ_ID_LOCATION = 6;
    private static final int OBJ_ID_PARKING_SPOT = 32700;
    private static final int OBJ_ID_VEHICLE_COUNTER = 32701;
    private static final String RES_STATE_FREE = "Free";
    private static final String RES_STATE_RESERVED = "Reserved";
    private static final String RES_STATE_OCCUPIED = "Occupied";
    private static final int RES_LATITUDE = 0;
    private static final int RES_LONGITUDE = 1;
    private static final int RES_PARKING_SPOT_STATE = 32801;
    private static final int RES_VEHICLE_COUNTER = 32803;
    private static final int RES_DIRECTION = 32805;

    private static final Map<String, Color> RES_STATE_COLOR = new HashMap<String, Color>() {
        private static final long serialVersionUID = 3844462182731417274L;

        {
            put(null, Color.of(0, 0, 0));
            put(RES_STATE_FREE, Color.GREEN);
            put(RES_STATE_RESERVED, Color.of(255, 165, 0));
            put(RES_STATE_OCCUPIED, Color.RED);
        }
    };

    private final SenseHat senseHat = new SenseHat();

    private static class Location {
        final public int x;
        final public int y;

        public Location(final int x, final int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "Location [x=" + x + ", y=" + y + "]";
        }

    }

    public static class State {
        public final Location location;
        public final String status;
        public final Long counter;
        public final String direction;

        public State() {
            location = null;
            status = null;
            counter = null;
            direction = null;
        }

        public State(final Location location, final String status, final Long counter, final String direction) {
            this.location = location;
            this.status = status;
            this.counter = counter;
            this.direction = direction;
        }

        public State withLocation(final Location location) {
            return new State(location, status, counter, direction);
        }

        public State withStatus(final String status) {
            return new State(location, status, counter, direction);
        }

        public State withCounter(final Long counter) {
            return new State(location, status, counter, direction);
        }

        public State withDirection(final String direction) {
            return new State(location, status, counter, direction);
        }

        @Override
        public String toString() {
            return "State [counter=" + counter + ", direction=" + direction + ", location=" + location + ", status="
                    + status + "]";
        }

    }

    private final ConcurrentMap<String, State> registrations = new ConcurrentHashMap<>();

    public SenseHatListener() {
        senseHat.ledMatrix.clear();
    }

    @Override
    public void objectAdded(final Registration registration, final LwM2mPath path) {
        // No-Op
    }

    private void redraw(final Set<Map.Entry<String, State>> entries) {
        Color[] colors = new Color[64];
        for (int i = 0; i < 64; ++i) {
            colors[i] = Color.of(0, 0, 0);
        }
        for (final Map.Entry<String, State> entry : entries) {
            final State state = entry.getValue();

            if (state.counter != null && state.direction != null) {
                senseHat.ledMatrix.showMessage(String.format("%s %s", state.counter, state.direction));
            }

            if (state.location != null && state.status != null) {
                colors[state.location.x * 8 + state.location.y] = RES_STATE_COLOR.get(state.status);
            }
        }
        senseHat.ledMatrix.setPixels(colors);
    }

    @Override
    public void objectUpdated(final Registration registration, final LwM2mPath path) {
        // No-Op
    }

    @Override
    public void objectUpdated(final Registration registration, final LwM2mPath path, final LwM2mNode node) {
        final String registrationId = registration.getId();
        final State state = registrations.getOrDefault(registrationId, new State());
        final int objectId = path.getObjectId();

        node.accept(new LwM2mNodeVisitor() {

            @Override
            public void visit(final LwM2mObject object) {
                // No-Op
            }

            @Override
            public void visit(final LwM2mObjectInstance instance) {
                final State newState;
                switch (objectId) {
                case OBJ_ID_LOCATION: {
                    final double x = (Double) instance.getResource(RES_LATITUDE).getValue();
                    final double y = (Double) instance.getResource(RES_LONGITUDE).getValue();
                    newState = state.withLocation(new Location((int) x, (int) y));
                    break;
                }
                case OBJ_ID_PARKING_SPOT: {
                    final String status = (String) instance.getResource(RES_PARKING_SPOT_STATE).getValue();
                    newState = state.withStatus(status);
                    break;
                }
                case OBJ_ID_VEHICLE_COUNTER: {
                    final long counter = (Long) instance.getResource(RES_VEHICLE_COUNTER).getValue();
                    final String direction = ((Long) instance.getResource(RES_DIRECTION).getValue()) == 0 ? "<out>"
                            : "<in>";
                    newState = state.withCounter(counter).withDirection(direction);
                    break;
                }
                default:
                    return;
                }

                LOG.info("Status for {} updated to {}", registrationId, newState);
                registrations.put(registrationId, newState);
                EXECUTOR.submit(() -> redraw(registrations.entrySet()));
            }

            @Override
            public void visit(final LwM2mResource resource) {
                // No-Op
            }
        });
    }

    @Override
    public void objectRemoved(final Registration registration, final LwM2mPath path) {
        registrations.remove(registration.getId());
        EXECUTOR.submit(() -> redraw(registrations.entrySet()));
    }

}