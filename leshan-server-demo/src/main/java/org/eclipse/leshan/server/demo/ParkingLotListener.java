package org.eclipse.leshan.server.demo;

import org.eclipse.leshan.core.node.LwM2mNode;
import org.eclipse.leshan.core.node.LwM2mPath;
import org.eclipse.leshan.server.registration.Registration;

public interface ParkingLotListener {
    void objectAdded(final Registration registration, final LwM2mPath path);

    void objectUpdated(final Registration registration, final LwM2mPath path);

    void objectUpdated(final Registration registration, final LwM2mPath path, final LwM2mNode node);

    void objectRemoved(final Registration registration, final LwM2mPath path);
}