package org.eclipse.leshan.server.demo.servlet;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.io.IOUtils;
import org.eclipse.leshan.core.request.WriteRequest;
import org.eclipse.leshan.core.response.WriteResponse;
import org.eclipse.leshan.server.californium.LeshanServer;
import org.eclipse.leshan.server.demo.model.Reservation;
import org.eclipse.leshan.server.demo.repository.ReservationRepository;
import org.eclipse.leshan.server.registration.Registration;
import org.glassfish.jersey.uri.UriTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service HTTP REST API calls.
 */
public class ReservationServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(ReservationServlet.class);
    private static final String NOT_FOUND = "{\"error\":\"not_found\"}";
    private static final long TIMEOUT = 5000;

    private static final int OBJ_ID_PARKING_SPOT = 32700;
    private static final int RES_PARKING_SPOT_STATE = 32801;

    private static final long serialVersionUID = 1L;
    private static final UriTemplate getEntityTemplate = new UriTemplate("/{id}");

    private final LeshanServer server;
    private final ReservationRepository reservationRepository;
    private final Gson gson;

    public ReservationServlet(final LeshanServer server, final ReservationRepository reservationRepository) {
        this.server = server;
        this.reservationRepository = reservationRepository;

        final GsonBuilder gsonBuilder = new GsonBuilder();
        this.gson = gsonBuilder.create();
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        final Map<String, String> parameters = new HashMap<>();
        final String prePath = req.getPathInfo();
        final String path = prePath == null ? "" : prePath.replaceAll("/$", "");

        if (path.isEmpty()) {
            found(resp, reservationRepository.get());

            return;
        }

        if (getEntityTemplate.match(path, parameters)) {
            final Long id = Long.valueOf(parameters.get("id"));
            final Optional<Reservation> reservation = reservationRepository.getByID(id);
            if (reservation.isPresent()) {
                found(resp, reservation.get());

                return;
            }
        }

        notFound(resp);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        final String prePath = req.getPathInfo();
        final String path = prePath == null ? "" : prePath.replaceAll("/$", "");

        if (path.isEmpty()) {
            final String body = IOUtils.toString(req.getInputStream(), StandardCharsets.UTF_8);
            final Reservation reservation = gson.fromJson(body, Reservation.class);
            reservationRepository.save(reservation);

            try {
                setReserved(reservation.getSpotName());
            } catch (final InterruptedException e) {
                LOG.error("Caught {}", e);
            }

            found(resp, reservation);

            return;
        }

        notFound(resp);
    }

    private void setReserved(final String parkingSpot) throws InterruptedException {
        final Registration registration = server.getRegistrationService().getByEndpoint(parkingSpot);
        if (registration == null) {
            return;
        }

        final WriteRequest request = new WriteRequest(OBJ_ID_PARKING_SPOT, 0, RES_PARKING_SPOT_STATE, "Reserved");
        final WriteResponse response = server.send(registration, request, TIMEOUT);

        LOG.info("Reserved response: {}", response);
    }

    @Override
    protected void doPut(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        final Map<String, String> parameters = new HashMap<>();
        final String prePath = req.getPathInfo();
        final String path = prePath == null ? "" : prePath.replaceAll("/$", "");

        if (getEntityTemplate.match(path, parameters)) {
            final Long id = Long.valueOf(parameters.get("id"));

            final String body = IOUtils.toString(req.getInputStream(), StandardCharsets.UTF_8);
            final Reservation reservation = gson.fromJson(body, Reservation.class).withID(id);
            reservationRepository.update(reservation);

            found(resp, reservation);

            return;
        }

        notFound(resp);
    }

    @Override
    protected void doDelete(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        final Map<String, String> parameters = new HashMap<>();
        final String prePath = req.getPathInfo();
        final String path = prePath == null ? "" : prePath.replaceAll("/$", "");

        if (getEntityTemplate.match(path, parameters)) {
            final Long id = Long.valueOf(parameters.get("id"));

            final Reservation reservation = (new Reservation()).withID(id);
            reservationRepository.delete(reservation);

            found(resp, new Object());

            return;
        }

        notFound(resp);
    }

    private void found(final HttpServletResponse resp, final Object result) throws IOException {
        final String json = gson.toJson(result);
        resp.setContentType("application/json");
        resp.getOutputStream().write(json.getBytes(StandardCharsets.UTF_8));
        resp.setStatus(HttpServletResponse.SC_OK);
    }

    private void notFound(final HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.getOutputStream().write(NOT_FOUND.getBytes(StandardCharsets.UTF_8));
        resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }
}