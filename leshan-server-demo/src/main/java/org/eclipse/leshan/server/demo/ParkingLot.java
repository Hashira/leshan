package org.eclipse.leshan.server.demo;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiConsumer;

import org.eclipse.leshan.Link;
import org.eclipse.leshan.core.node.LwM2mNode;
import org.eclipse.leshan.core.node.LwM2mPath;
import org.eclipse.leshan.core.observation.Observation;
import org.eclipse.leshan.core.request.DownlinkRequest;
import org.eclipse.leshan.core.request.ObserveRequest;
import org.eclipse.leshan.core.request.WriteRequest;
import org.eclipse.leshan.core.response.LwM2mResponse;
import org.eclipse.leshan.core.response.ObserveResponse;
import org.eclipse.leshan.server.californium.LeshanServer;
import org.eclipse.leshan.server.observation.ObservationListener;
import org.eclipse.leshan.server.registration.Registration;
import org.eclipse.leshan.server.registration.RegistrationListener;
import org.eclipse.leshan.server.registration.RegistrationUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParkingLot implements ObservationListener, RegistrationListener {
    private static final Logger LOG = LoggerFactory.getLogger(ParkingLot.class);
    private static final long TIMEOUT = 5000;

    private static final int OBJ_ID_PARKING_SPOT = 32700;
    private static final int OBJ_ID_VEHICLE_COUNTER = 32701;
    private static final int RES_LOT_NAME = 32802;

    private final LeshanServer server;
    private final String lotName;

    private final Set<ParkingLotListener> listeners = new HashSet<>();
    private final ConcurrentMap<String, Set<LwM2mPath>> pathMapping = new ConcurrentHashMap<>();

    private void parseLinks(final Link[] links, final BiConsumer<Integer, Integer> f) {
        for (final Link link : links) {
            final String[] stringParts = link.getUrl().split("/");
            if (stringParts.length != 3) {
                continue;
            }
            final Integer[] parts = Arrays.asList(stringParts).stream().skip(1).map(Integer::parseInt)
                    .toArray(Integer[]::new);

            f.accept(parts[0], parts[1]);
        }
    }

    private Set<LwM2mPath> getActivePaths(final String id) {
        return pathMapping.computeIfAbsent(id, k -> Collections.newSetFromMap(new ConcurrentHashMap<>()));
    }

    @Override
    public void registered(final Registration reg, final Registration previousReg,
            final Collection<Observation> previousObsersations) {

        final Set<LwM2mPath> activePaths = getActivePaths(reg.getId());
        parseLinks(reg.getObjectLinks(), (objectId, instanceId) -> {
            final LwM2mPath path = new LwM2mPath(objectId, instanceId);
            if (activePaths.contains(path)) {
                return;
            }

            activePaths.add(path);
            notifyObjectAdded(reg, path);

            submitRequest(reg, new ObserveRequest(objectId, instanceId),
                    chainHandlers(this::logRoundTrip, this::receiveObserve));

            switch (objectId) {
            case OBJ_ID_PARKING_SPOT:
            case OBJ_ID_VEHICLE_COUNTER:
                submitRequest(reg, new WriteRequest(objectId, instanceId, RES_LOT_NAME, lotName), this::logRoundTrip);
                break;
            }
        });
    }

    @Override
    public void updated(final RegistrationUpdate update, final Registration updatedReg,
            final Registration previousReg) {
        final Set<LwM2mPath> previous = new HashSet<>();
        final Set<LwM2mPath> updated = new HashSet<>();

        parseLinks(previousReg.getObjectLinks(), (objectId, instanceId) -> {
            previous.add(new LwM2mPath(objectId, instanceId));
        });

        parseLinks(updatedReg.getObjectLinks(), (objectId, instanceId) -> {
            updated.add(new LwM2mPath(objectId, instanceId));
        });

        final Set<LwM2mPath> activePaths = getActivePaths(updatedReg.getId());
        for (final LwM2mPath path : previous) {
            if (updated.contains(path)) {
                continue;
            }

            activePaths.remove(path);
            notifyObjectRemoved(previousReg, path);
        }

        for (final LwM2mPath path : updated) {
            if (previous.contains(path)) {
                continue;
            }

            activePaths.add(path);
            notifyObjectAdded(updatedReg, path);

            final int objectId = path.getObjectId();
            final int instanceId = path.getObjectInstanceId();
            submitRequest(updatedReg, new ObserveRequest(objectId, instanceId),
                    chainHandlers(this::logRoundTrip, this::receiveObserve));
        }
    }

    @Override
    public void unregistered(final Registration reg, final Collection<Observation> observations, final boolean expired,
            final Registration newReg) {
        final Set<LwM2mPath> activePaths = getActivePaths(reg.getId());
        parseLinks(reg.getObjectLinks(), (objectId, instanceId) -> {
            final LwM2mPath path = new LwM2mPath(objectId, instanceId);
            activePaths.remove(path);
            notifyObjectRemoved(reg, path);
        });
    }

    @Override
    public void newObservation(final Observation observation, final Registration registration) {
        // No-Op
    }

    @Override
    public void cancelled(final Observation observation) {
        final String registrationId = observation.getRegistrationId();
        final Set<LwM2mPath> activePaths = getActivePaths(registrationId);
        final LwM2mPath path = observation.getPath();
        if (!activePaths.contains(path)) {
            return;
        }

        final Registration registration = server.getRegistrationService().getById(registrationId);
        if (registration == null) {
            return;
        }

        final Set<Observation> activeObservations = server.getObservationService().getObservations(registration);
        for (final Observation activeObservation : activeObservations) {
            if (!activeObservation.getPath().equals(path)) {
                continue;
            }

            LOG.debug("[{}] Observe takeover occured for {}", registrationId, path);
            return;
        }

        LOG.debug("[{}] Requesting a new observation for {}", registrationId, path);
        submitRequest(registration, new ObserveRequest(null, path.toString()),
                chainHandlers(this::logRoundTrip, this::receiveObserve));
    }

    @Override
    public void onResponse(final Observation observation, final Registration registration,
            final ObserveResponse response) {
        final Set<LwM2mPath> activePaths = getActivePaths(registration.getId());
        final LwM2mPath path = observation.getPath();
        if (!activePaths.contains(path)) {
            return;
        }

        LOG.debug("[{}] Received update: {}", registration.getId(), response);

        final LwM2mNode node = response.getContent();

        notifyObjectUpdated(registration, path, node);
    }

    @Override
    public void onError(final Observation observation, final Registration registration, final Exception error) {
        LOG.error("[{}] Exception encountered: {}", registration.getId(), error);
        error.printStackTrace();
    }

    private void notifyObjectAdded(final Registration registration, final LwM2mPath path) {
        for (final ParkingLotListener listener : listeners) {
            listener.objectAdded(registration, path);
        }
    }

    private void notifyObjectUpdated(final Registration registration, final LwM2mPath path) {
        for (final ParkingLotListener listener : listeners) {
            listener.objectUpdated(registration, path);
        }
    }

    private void notifyObjectUpdated(final Registration registration, final LwM2mPath path, final LwM2mNode node) {
        for (final ParkingLotListener listener : listeners) {
            listener.objectUpdated(registration, path, node);
        }
    }

    private void notifyObjectRemoved(final Registration registration, final LwM2mPath path) {
        for (final ParkingLotListener listener : listeners) {
            listener.objectRemoved(registration, path);
        }
    }

    public void addListener(final ParkingLotListener listener) {
        listeners.add(listener);
    }

    public void removeListener(final ParkingLotListener listener) {
        listeners.remove(listener);
    }

    public ParkingLot(final LeshanServer server, final String lotName) {
        this.server = server;
        this.lotName = lotName;

        server.getRegistrationService().addListener(this);
        server.getObservationService().addListener(this);
    }

    void receiveObserve(final ObserveRequest request, final ObserveResponse response) {
        final Observation observation = response.getObservation();
        final Registration registration = server.getRegistrationService().getById(observation.getRegistrationId());
        final LwM2mPath path = observation.getPath();
        final LwM2mNode node = response.getContent();

        notifyObjectUpdated(registration, path, node);
    }

    private <E extends LwM2mResponse> void logRoundTrip(final DownlinkRequest<E> request, final E response) {
        LOG.debug("Got {} as response to {}", response, request);
    }

    private <E extends LwM2mResponse, F extends DownlinkRequest<E>> void noopHandler(final F f, final E e) {
    }

    @SafeVarargs
    private final <E extends LwM2mResponse, F extends DownlinkRequest<E>> BiConsumer<F, E> chainHandlers(
            final BiConsumer<F, E> first, final BiConsumer<F, E>... consumers) {
        BiConsumer<F, E> result = first;
        for (final BiConsumer<F, E> consumer : consumers) {
            result = result.andThen(consumer);
        }
        return result;
    }

    private <E extends LwM2mResponse, F extends DownlinkRequest<E>> void submitRequest(final Registration reg,
            final F request, final BiConsumer<F, E> responseHandler) {
        try {
            final E response = server.send(reg, request, TIMEOUT);
            responseHandler.accept(request, response);
        } catch (final InterruptedException e) {
            LOG.error("Caught {}", e);
            return;
        }
    }
}