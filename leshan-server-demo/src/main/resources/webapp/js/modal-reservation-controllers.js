/*******************************************************************************
 * Copyright (c) 2013-2015 Sierra Wireless and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * 
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 * 
 * Contributors:
 *     Sierra Wireless - initial API and implementation
 *******************************************************************************/

angular.module('modalReservationControllers', [])

    .controller('modalReservationController', [
        '$scope',
        '$modalInstance',
        'parkingSpots',
        'parkingLots',
        function ($scope, $modalInstance, parkingSpots, parkingLots) {

            $scope.parkingSpots = parkingSpots;
            $scope.parkingLots = parkingLots;

            // Define button function 
            $scope.submit = function () {
                $modalInstance.close({
                    plateNumber: $scope.plateNumber,
                    spotName: $scope.spotName,
                    lotName: $scope.lotName
                });
            };
            $scope.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
