/*******************************************************************************
 * Copyright (c) 2013-2015 Sierra Wireless and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * 
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 * 
 * Contributors:
 *     Sierra Wireless - initial API and implementation
 *     Achim Kraus (Bosch Software Innovations GmbH) - fix typo in notificationCallback
 *                                                     processing multiple resources
 *******************************************************************************/

var lwReservationControllers = angular.module('reservationControllers', []);

lwReservationControllers.controller('ReservationListCtrl', [
    '$scope',
    '$http',
    '$location',
    '$modal',
    function ReservationListCtrl($scope, $http, $location, $modal) {

        // update navbar
        angular.element("#navbar").children().removeClass('active');
        angular.element("#reservation-navlink").addClass('active');

        // add function to show reservation
        $scope.showReservation = function (reservation) {
            $location.path('/reservations/' + reservation.id);
        };

        // get the list of parking spots
        $http.get('api/parkingLot/parkingSpots').error(function (data, status, headers, config) {
            $scope.error = "Unable to get spots list: " + status + " " + data;
            console.error($scope.error);
        }).success(function (data, status, headers, config) {
            $scope.parkingSpots = data;
            $scope.parkingSpotsList = true;
        });

        // get the list of parking lots
        $http.get('api/parkingLot/parkingLots').error(function (data, status, headers, config) {
            $scope.error = "Unable to get lots list: " + status + " " + data;
            console.error($scope.error);
        }).success(function (data, status, headers, config) {
            $scope.parkingLots = data;
            $scope.parkingLotsList = true;
        });

        // get the list of reservation
        $http.get('api/reservations').error(function (data, status, headers, config) {
            $scope.error = "Unable to get reservation list: " + status + " " + data;
            console.error($scope.error);
        }).success(function (data, status, headers, config) {
            $scope.reservations = data;
            $scope.reservationslist = true;

            $scope.addReservation = function () {
                var modalInstance = $modal.open({
                    templateUrl: 'partials/modal-reservation.html',
                    controller: 'modalReservationController',
                    resolve: {
                        parkingSpots: function () { return $scope.parkingSpots; },
                        parkingLots: function () { return $scope.parkingLots; }
                    }
                });

                modalInstance.result.then(function (reservation) {
                    $http({
                        method: 'POST', url: "api/reservations", data: reservation
                    })
                        .success(function (data, status, headers, config) {
                            $scope.reservations.push(data);
                        }).error(function (data, status, headers, config) {
                            errormessage = "Unable to delete reservation " + reservation.id + " : " + status + " " + data;
                            dialog.open(errormessage);
                            console.error(errormessage);
                        });
                });
            }

            $scope.delReservation = function (reservation) {
                var uri = "api/reservations/" + reservation.id;
                $http.delete(uri)
                    .success(function (data, status, headers, config) {
                        var i = $scope.reservations.indexOf(reservation);
                        if (i != -1) {
                            $scope.reservations.splice(i, 1);
                        }
                    }).error(function (data, status, headers, config) {
                        errormessage = "Unable to delete reservation " + reservation.id + " : " + status + " " + data;
                        dialog.open(errormessage);
                        console.error(errormessage);
                    });
            };
        });
    }]);
